"""
#####################################################################
#   Creation of Blast databases SwissProt and TrEMBL for Viridiplantae
#
#   @author Waldeyr Mendes Cordeiro da Silva
#   @version Ipê amarelo
#   @place Germany, Leipzig
#   @date 2017-September
#####################################################################
"""

from classes.Config2Path import *


class RunBlastConfiguration(Config2Path):
    """Creation of Blast databases SwissProt and TrEMBL for Viridiplantae"""

    def __init__(self):
        """
        Construct a new object.
        :return: returns nothing
        """
        Config2Path.__init__(self)

    def run(self):
        print(self.TREE)
        print("Downloading data...")
        os.system("wget -P "+self.BLASTDB_PATH+" -q --show-progress https://www.dropbox.com/s/9cpxe3934dmxqv2/blastdb.zip")
        os.system("unzip "+self.BLASTDB_PATH+"/blastdb.zip -d "+self.BLASTDB_PATH)
        os.system("rm "+self.BLASTDB_PATH+"/blastdb.zip")
        SwissProtFasta = self.BLASTDB_PATH + "/SwissProt.fasta"
        SwissProtDb = self.BLASTDB_PATH + "/SwissProt"
        TrEMBLFasta = self.BLASTDB_PATH + "/TrEMBL.fasta"
        TrEMBLDb = self.BLASTDB_PATH + "/TrEMBL"
        cmdSwissProt = "makeblastdb -dbtype prot -in %s -out %s" % (SwissProtFasta, SwissProtDb)
        cmdTrEMBL = "makeblastdb -dbtype prot -in %s -out %s" % (TrEMBLFasta, TrEMBLDb)
        os.system(cmdSwissProt)
        os.system(cmdTrEMBL)


runnner = RunBlastConfiguration()
runnner.run()
