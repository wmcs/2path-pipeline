# 2path #

Terpenoids are involved in interactions such as signalling for communication intra/inter species, signalling molecules to attract pollinating insects, and defence against herbivores and microbes. 
Owing to their chemical composition, many terpenoids possess vast pharmacological applicability in medicine and biotechnology, besides important roles in ecology, industry and commerce. 
Metabolic networks are composed of metabolic pathways, they allow us to represent the metabolism of an organism. 
The biosynthesis of terpenes has been widely studied over the years, and it is well known that they can be synthesised from two metabolic pathways: mevalonate pathway (MVA) and non-mevalonate pathway (MEP). 
On the other hand, genome-scale reconstruction of metabolic networks faces many challenges, including organisational data storage and data modelling, to properly represent the complexity of systems biology. 
Recent NoSQL database paradigms have introduced new concepts of scalable storage and data queries. 
With regard to biological data, the use of graph databases has grown because of its versatility. 
We propose 2Path, a graph database designed to represent terpenoid metabolic networks. 
It is modelled in such a way so that it preserves important terpenoid biosynthesis characteristics.
Also, we provide python scripts to easly reconstruct the terpenoid metabolic network of plant organisms from their genomic data.

### What is this 2path for? ###

* To a graph database based exploration of terpenoid network
* to a terpenoid network reconstruction for a given plant organism 

### How do I get set up? ###

[2path wiki](https://bitbucket.org/wmcs/2path/wiki/Home)

### Author ###

Waldeyr Mendes Cordeiro da Silva

### License? ###

Apache2
