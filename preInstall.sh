#!/bin/bash
clear
if [[ -r /etc/os-release ]]; then
	. /etc/os-release
	if [[ $ID = fedora ]] || [[ $ID = centos ]]; then
	    dnf install wget curl -y
		dnf install perl -y
		if [ -e /usr/bin/blastp ]
		then
		    echo "Blast already installed."
		else
		    wget -q --show-progress ftp://ftp.ncbi.nlm.nih.gov/blast/executables/LATEST/ncbi-blast-2.6.0+-1.x86_64.rpm
		    dnf install ncbi-blast-2.6.0+-1.x86_64.rpm -y
		    rm ncbi-blast-2.6.0+-1.x86_64.rpm
		fi
		dnf install java-1.8.0-openjdk -y
		dnf install python-pip python-wheel -y
		dnf install zip unzip -ys
		dnf install git -y
		python3 -m pip install pip --upgrade
		python3 -m pip install setuptools --upgrade
		python3 -m pip install pandas
		python3 -m pip install pylint
		python3 -m pip install biopython --upgrade
		python3 -m pip install bioservices
		python3 -m pip install pyorient
		python3 -m pip install configparser
		python3 -m pip install pathlib
		python3 -m pip install datetime
	else
		if [[ $ID = ubuntu ]] || [[ $ID = debian ]]; then
	    	apt-get install wget curl -y
            apt-get install perl -y
            apt-get install ncbi-blast+ -y
            apt-get install java-1.8.0-openjdk -y
            apt-get install python-pip python-wheel -y
            apt-get install zip unzip -y
            apt-get install git -y
            apt-get install python3-setuptools -y
            easy_install3 pip
            python3 -m pip install pip --upgrade
            python3 -m pip install setuptools --upgrade
            python3 -m pip install pandas
            python3 -m pip install pylint
            python3 -m pip install biopython --upgrade
            python3 -m pip install bioservices
            python3 -m pip install pyorient
            python3 -m pip install configparser
            python3 -m pip install pathlib
            python3 -m pip install datetime
		fi
	fi
else
    echo "Running an unplanned distribution. Dependencies must be installed manually."
fi

# Prompt for a user choice about the orientDB:
echo "Would you like to download an OrientDB instance with 2path embeded (y or n) ?"
read ORIENTDB_CHOICE
if [ "$ORIENTDB_CHOICE" == "y" ]; then
    echo "Installing OrientDB..."
	wget -q --show-progress https://www.dropbox.com/s/77a7nkxnwoigixv/orientdb-2path.zip
	unzip -q orientdb-2path.zip
	./orientdb-2path/bin/server.sh &
	rm orientdb-2path.zip
fi
clear
echo "2path dependencies sucessifully installed!"

re='^[0-9]+$'
if [[ $(ps aux | grep "orientdb" | grep -v grep | awk '{print $2}') =~ $re ]]; then
	echo  "OrientDB running."
fi
echo "Please run: python3 install.py"