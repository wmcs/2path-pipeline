"""
#####################################################################
#   Creation of Blast databases SwissProt and TrEMBL for Viridiplantae
#
#   @author Waldeyr Mendes Cordeiro da Silva
#   @version Ipê amarelo
#   @place Germany, Leipzig
#   @date 2017-September
#####################################################################
"""

from classes.Config2Path import *
from runDatabaseConfiguration import RunDatabaseConfiguration


class RunFirstTime(Config2Path):
    """Creation of Blast databases SwissProt and TrEMBL for Viridiplantae"""

    def __init__(self):
        """
        Construct a new object.
        :return: returns nothing
        """
        Config2Path.__init__(self)

    def run(self):
        #WARNNING
        print(self.WARNING+"WARNNING\n")
        print ("Have you installed all dependencies and OrientDB?")
        print ("If not, please consider running preInstall.sh before." + self.ENDC)

        #create config folder
        os.system("mkdir config")

        #Blast data and configuration
        print("Downloading BLAST data...")
        os.system("wget -P "+self.BLASTDB_PATH+" -q --show-progress https://www.dropbox.com/s/9cpxe3934dmxqv2/blastdb.zip")
        os.system("unzip -q "+self.BLASTDB_PATH+"/blastdb.zip -d "+self.BLASTDB_PATH)
        os.system("rm "+self.BLASTDB_PATH+"/blastdb.zip")
        SwissProtFasta = self.BLASTDB_PATH + "/SwissProt.fasta"
        SwissProtDb = self.BLASTDB_PATH + "/SwissProt"
        TrEMBLFasta = self.BLASTDB_PATH + "/TrEMBL.fasta"
        TrEMBLDb = self.BLASTDB_PATH + "/TrEMBL"
        cmdSwissProt = "makeblastdb -dbtype prot -in %s -out %s" % (SwissProtFasta, SwissProtDb)
        cmdTrEMBL = "makeblastdb -dbtype prot -in %s -out %s" % (TrEMBLFasta, TrEMBLDb)
        os.system(cmdSwissProt)
        os.system(cmdTrEMBL)
        os.system("rm " + TrEMBLFasta)
        os.system("rm "+ SwissProtFasta)

        #Example data download and configure
        print("Downloading Copaifera demo data...")
        os.system(
            "wget -P " + self.INPUT_PATH + " -q --show-progress https://www.dropbox.com/s/h1favr972vfb9xh/input.zip")
        os.system("unzip -q " + self.INPUT_PATH + "/input.zip -d " + self.INPUT_PATH)
        os.system("rm " + self.INPUT_PATH + "/input.zip")

runnner = RunFirstTime()
runnner.run()
