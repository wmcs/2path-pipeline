"""
########################################################################
#   Config2Path is a class that provide global configurations for 2Path
#
#   @author Waldeyr Mendes Cordeiro da Silva
#   @version Ipê amarelo
#   @place Germany, Leipzig
#   @date 2017-Septmeber
#########################################################################
"""

import os
import sys
import re
import configparser
import pyorient
import time
import random
from ftplib import FTP
from pathlib import Path
from Bio import Entrez
from bioservices import *
from Bio.Blast.Applications import NcbiblastpCommandline
from Bio.Blast.Applications import NcbiblastxCommandline
from Bio.Blast import NCBIXML
from Bio.KEGG.KGML.KGML_parser import read
from Bio.KEGG.KGML.KGML_pathway import Reaction
from datetime import timedelta

'''https://www.ncbi.nlm.nih.gov/books/NBK279675/'''


class Config2Path:
    """Config2Path is a class that provide global configurations for 2Path"""

    HEADER = '\033[95m'
    BLUE = '\033[94m'
    GREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'

    INPUT_ARROW = BLUE + ">--> " + ENDC
    INPUT_INVALID_MSG = WARNING + "Invalid input!" + ENDC

    PATH = os.path.abspath(os.path.dirname(__file__))
    DATABASE_CONFIG_PATH = os.path.join(PATH, "../config", "2Path.ini")
    ORGANISM_CONFIG_PATH = os.path.join(PATH, "../config")
    INPUT_PATH = os.path.join(PATH, "../input")
    OUTPUT_PATH = os.path.join(PATH, "../output")
    BLASTDB_PATH = os.path.join(PATH, "../blastdb")

    HEAD = HEADER + "\n\n.---. .---.       .-. .-.    \n"
    HEAD += "`--. :: .; :     .' `.: :    \n"
    HEAD += "  ,',':  _.'.--. `. .': `-.  \n"
    HEAD += ".'.'_ : :  ' .; ; : : : .. : \n"
    HEAD += ":____;:_;  `.__,_;:_; :_;:_; \n\n" + ENDC

    TREE = WARNING + "       _-_\n"
    TREE += "    /~~   ~~\\\n"
    TREE += " /~~         ~~\\\n"
    TREE += "{   ~~   ~~     }\n"
    TREE += " \  _-     -_  /    " + GREEN + \
            ".---. .---.       .-. .-.     \n"
    TREE += "       \\ //         " + "`--. :: .; :     .' `.: :     \n"
    TREE += "       | |          " + "  ,',':  _.'.--. `. .': `-.     \n"
    TREE += "       | |          " + ".'.'_ : :  ' .; ; : : : .. :  \n"
    TREE += "      // \\\\         :____;:_;  `.__,_;:_; :_;:_;  \n" + ENDC

    msgs = [
        "Visit the Botanical Garden of Rio de Janeiro: http://www.jbrj.gov.br",
        "Handroanthus is a beautiful genus of flowering plants in the family Bignoniaceae. In portuguese, the popular name is Ipê",
        "Caesalpinia echinata is the scientific name of the plant who gives the name to Brazil. The popular name is Pau Brasil"
    ]
