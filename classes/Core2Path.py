"""
#########################################################################
#   Core is a class that provide the 2path core creation or manipulation
#
#   @author Waldeyr Mendes Cordeiro da Silva
#   @version Ipê amarelo
#   @place Germany, Leipzig
#   @date 2017-September
#########################################################################
"""

from classes.Config2Path import *


class Core2Path(Config2Path):
    '''
    Core is a class that provide the 2path core creation ou manipulation 
    '''

    def __init__(self, client):
        """
        Construct a new 'Core' object.
        :return: returns nothing
        """
        self._client = client
        Config2Path.__init__(self)

    def loadKeggData(self):
        '''
        This method retrieve terpenoids data from KEGG and insert them into 2path
        It uses:
            python bioservices
            pyorient
        '''
        mapListFromKegg = {}
        mapListFromKegg['rn00900'] = "Terpenoid backbone biosynthesis"
        mapListFromKegg['rn00902'] = "Monoterpenoid biosynthesis"
        mapListFromKegg['rn00909'] = "Sesquiterpenoid and triterpenoid biosynthesis"
        mapListFromKegg['rn00904'] = "Diterpenoid biosynthesis"
        mapListFromKegg['rn00906'] = "Carotenoid biosynthesis"
        mapListFromKegg['rn00905'] = "Brassinosteroid biosynthesis"
        mapListFromKegg['rn00981'] = "Insect hormone biosynthesis"
        mapListFromKegg['rn00908'] = "Zeatin biosynthesis"
        mapListFromKegg['rn00903'] = "Limonene and pinene degradation"
        mapListFromKegg['rn00281'] = "Geraniol degradation"
        mapListFromKegg['rn00522'] = "Biosynthesis of 12-, 14- and 16-membered macrolides"
        mapListFromKegg['rn01051'] = "Biosynthesis of ansamycins"
        mapListFromKegg['rn01059'] = "Biosynthesis of enediyne antibiotics"
        mapListFromKegg['rn01056'] = "Biosynthesis of type II polyketide backbone"
        mapListFromKegg['rn01057'] = "Biosynthesis of type II polyketide products"
        mapListFromKegg['rn00253'] = "Tetracycline biosynthesis"
        mapListFromKegg['rn00523'] = "Polyketide sugar unit biosynthesis"
        mapListFromKegg['rn01053'] = "Biosynthesis of siderophore group nonribosomal peptides"
        mapListFromKegg['rn01055'] = "Biosynthesis of vancomycin group antibiotics"

        '''Initialising Kegg service'''
        kegg = KEGG()
        for mapId, mapDesc in mapListFromKegg.items():
            print("\nWorking on KEGG map: " + mapDesc)
            res = kegg.get(mapId, "kgml")
            map = read(res)
            reactionsList = map.reactions
            for reaction in reactionsList:
                print('.', end='', flush=True)
                reactionKeggId = reaction.name.split(":")[1]
                self.insertReaction(self._client, "keggId",
                                    reactionKeggId.strip())
                self.updateReaction(self._client, 'pathway',
                                    mapDesc, reactionKeggId)
                reactionData = kegg.parse(kegg.get(reactionKeggId))
                for key, value in reactionData.items():
                    if key == 'DBLINKS':
                        for db, dbValues in value.items():
                            rheaId = ""
                            if db == 'RHEA':
                                rheaId = dbValues.strip()
                                self.updateReaction(self._client, 'rheaId',
                                                    rheaId, reactionKeggId)

                for reactionSubstrate in reaction.substrates:
                    substrateKeggId = reactionSubstrate.name.split(":")[1]
                    self.insertCompound(
                        self._client, "keggId", substrateKeggId.strip())
                    self.insertEdgeSubstrateFor(
                        self._client, substrateKeggId.strip(), reactionKeggId.strip())

                for reactionProduct in reaction.products:
                    productKeggId = reactionProduct.name.split(":")[1]
                    self.insertCompound(
                        self._client, "keggId", productKeggId.strip())
                    self.insertEdgeProductOf(
                        self._client, reactionKeggId.strip(), productKeggId.strip())
        return

    def loadCheBI(self):
        """
        This method retrieves compounds data from CHEBI and insert them into 2path
        It uses:
            python bioservices
            pyorient
        """
        '''Initialising ChEBI service'''
        chebi = ChEBI()
        checkQtdQuery = "SELECT COUNT(*) as qtd FROM Compound"
        checkQtd = self._client.query(checkQtdQuery)
        for compoundsQtd in checkQtd:
            qtd = compoundsQtd.qtd
        query = "SELECT keggId FROM Compound LIMIT %s" % (qtd)
        dataCompound = self._client.query(query)
        print("\nWorking on CheBI data")
        print("\nUpdating Compounds...")
        for compound in dataCompound:
            print('.', end='', flush=True)
            '''Only the best result'''
            res = chebi.getLiteEntity(
                compound.keggId, searchCategory='ALL', maximumResults=1)
            if res:
                for entry in res:
                    chebiId = entry.chebiId
                    chebiRes = chebi.getCompleteEntity(chebiId)
                    self.updateCompound(
                        self._client, 'chebiId', chebiId, compound.keggId)
                    if 'smiles' in chebiRes:
                        chebiSmiles = chebiRes.smiles
                        self.updateCompound(self._client, 'chebiSmiles',
                                            chebiSmiles, compound.keggId)
                    if 'IupacNames' in chebiRes:
                        for IupacName in chebiRes.IupacNames:
                            iupacName = IupacName.data
                            self.updateCompound(self._client, 'iupacName',
                                                iupacName, compound.keggId)
                    if 'Formulae' in chebiRes:
                        for Formula in chebiRes.Formulae:
                            chebiFormula = Formula.data
                            self.updateCompound(self._client, 'chebiFormula',
                                                chebiFormula, compound.keggId)
                    if 'mass' in chebiRes:
                        chebiMass = chebiRes.mass
                        self.updateCompound(self._client, 'chebiMass',
                                            chebiMass, compound.keggId)

            else:
                self.updateCompound(
                    self._client, 'chebiSmiles', 'none', compound.keggId)
                self.updateCompound(self._client, 'iupacName',
                                    'none', compound.keggId)
                self.updateCompound(
                    self._client, 'chebiFormula', 'none', compound.keggId)
                self.updateCompound(self._client, 'chebiMass',
                                    'none', compound.keggId)

    def loadEnzymes(self):
        """
        This method retrieve Enzyme data from expasy.org and insert them into 2path
        The correspondence between KEGG data and Expasy data is made by kegg.link("enzyme", "reaction")
        It uses:
            python ftp
            python regex
            python bioservices
            pyorientI
        """
        ftp = FTP('ftp.expasy.org')  # connect to host, default port
        ftp.login()  # user anonymous, passwd anonymous@
        ftp.cwd('databases/enzyme/')  # change into "debian" directory
        # ftp.retrlines('LIST')          # list directory contents
        filename = 'enzclass.txt'
        ftp.retrbinary('RETR ' + filename, open(filename, 'wb').write)
        enzymesClasses = {}
        f = open(filename)
        lines = f.readlines()
        for line in lines:
            regex = r"^\d\.(\d+|\s)"
            pattern = re.compile(regex)
            if pattern.match(line.strip()):
                k = re.split("\s{2,}", line.strip())[0]
                k = k.replace(" ", "")
                v = re.split("\s{2,}", line.strip())[1]
                v = v.replace(".", "")
                enzymesClasses[k] = v
        f.close()

        kegg = KEGG()
        mapStringRaw = kegg.link("enzyme", "reaction")
        mapString = mapStringRaw.split('\n')
        regexClass = r"\d\.{1}\-\.{1}\-\.{1}\-"
        regexSubClass = r"\d\.{1}\d+\.{1}\-\.{1}\-"
        regexSubSubClass = r"\d\.{1}\d+\.{1}\d+\.{1}\-"
        patternClass = re.compile(regexClass)
        patternSubClass = re.compile(regexSubClass)
        patternSubSubClass = re.compile(regexSubSubClass)
        print("\nWorking on Enzymes...")
        count = 0
        for map in mapString:
            print("." if count % 2 == 0 else "", end='', flush=True)
            count += count
            if map:
                ec = map.split("	")[1].split(":")[1]
                ecClassification = ec.split(".")
                for k, v in enzymesClasses.items():
                    v = v.replace("'", "\\'")
                    refClassification = k.split(".")
                    if patternClass.match(k) and refClassification[0] == ecClassification[0]:
                        ecClass = v
                    if patternSubClass.match(k) and refClassification[0] == ecClassification[0] and refClassification[
                        1] == ecClassification[1]:
                        ecSubClass = v
                    if patternSubSubClass.match(k) and refClassification[0] == ecClassification[0] and \
                                    refClassification[1] == ecClassification[1] and refClassification[2] == \
                            ecClassification[2]:
                        ecSubSubClass = v
                self.insertEnzyme(self._client, ec, ecClass,
                                  ecSubClass, ecSubSubClass)

        print("\nWorking on Relationships between Enzymes and Reactions...")
        for map in mapString:
            print('.', end='', flush=True)
            if map:
                ec = map.split("	")[1].split(":")[1]
                keggId = map.split("	")[0].split(":")[1]
                self.insertEdgeCatalyse(self._client, ec, keggId)

    def rebuildEdgeCatalyse(self):
        """
        This method retrieve Enzyme data from expasy.org and rebuild Edge Catalyse into 2path
        The correspondence between KEGG data and Expasy data is made by kegg.link("enzyme", "reaction")
        This method is executed when we need to rebuild the CATALYSE Edge, after the user executes runUpdateDatabase()
        """

        kegg = KEGG()
        mapStringRaw = kegg.link("enzyme", "reaction")
        mapString = mapStringRaw.split('\n')

        for map in mapString:
            print('.', end='', flush=True)
            if map:
                ec = map.split("	")[1].split(":")[1]
                keggId = map.split("	")[0].split(":")[1]
                self.insertEdgeCatalyse(self._client, ec, keggId)

    def insertReaction(self, client, k, v):
        """This method inserts a record into a Reaction vertice after verifiyng if it does not exists 
        Args:
            param1 (obj): The client connection from DatabaseClass
            param2 (str): The key of propertie - k
            param3 (str): The value of propertie - v
        Returns:
            void: void
        Example:
            insertReaction(client, "keggId", "R00000")
        """
        verifyQuery = "SELECT COUNT(*) as qtd FROM Reaction WHERE keggId = '%s'" % (v)
        dataSubstrate = client.query(verifyQuery)
        for x in dataSubstrate:
            qtd = x.qtd
        if qtd == 0:
            client.command("INSERT INTO Reaction (" +
                           k + ") VALUES ('%s')" % (v))
        return

    def insertCompound(self, client, k, v):
        """This method inserts a Compound Vertice into 2path after verifiyng if it does not exists 
        Args:
            param1 (obj): The client connection from DatabaseClass.
            param2 (str): The key of propertie - k
            param2 (str): The value of propertie - v
        Returns:
            void: void
        Example:
            insertCompound(client, "compoundId", "C00000")
        """
        compoundNames = ""
        ks = KEGG()
        dict = ks.parse(ks.get(v))
        if dict:
            for key, value in dict.items():
                if key == 'NAME':
                    for name in value:
                        compoundNames += name
            compoundNames = compoundNames.replace("'", "\\'")
            verifyQuery = "SELECT COUNT(*) as qtd FROM Compound WHERE keggId = '%s'" % (v)
            dataSubstrate = client.query(verifyQuery)
            for qtdOfCompounds in dataSubstrate:
                qtd = qtdOfCompounds.qtd
            if qtd == 0:
                client.command("INSERT INTO Compound (" + k +
                               ", name) VALUES ('%s', '%s')" % (v, compoundNames.strip()))
        else:
            return
        return

    def insertEdgeSubstrateFor(self, client, source, target):
        """This method inserts a SUBSTRATE_FOR Edge into 2path after verifiyng if it does not exists 
        Args:
            param1 (obj): The client connection from DatabaseClass.
            param2 (str): The keggId of Compound source - source
            param2 (str): The keggId of Reaction target - target
        Returns:
            void: void
        Example:
            insertEdgeSubstrateFor(client, "C00000", "R00000")
        """
        sqlCheck = "SELECT EXPAND(IN('SUBSTRATE_FOR')) FROM (SELECT FROM Reaction WHERE keggId = '%s')" % (
            target)
        checkCompound = client.query(sqlCheck)
        if len(checkCompound) == 0:
            query = "CREATE EDGE SUBSTRATE_FOR FROM (SELECT * FROM Compound WHERE keggId = '%s') TO (SELECT * FROM Reaction WHERE keggId = '%s')" % (
                source, target)
            client.command(query)
            return
        else:
            return

    def insertEdgeProductOf(self, client, source, target):
        """This method inserts a PRODUCT_OF Edge into 2path after verifiyng if it does not exists 
        Args:
            param1 (obj): The client connection from DatabaseClass.
            param2 (str): The keggId of Reaction source - source
            param2 (str): The keggId of Compound target - target
        Returns:
            void: void
        Example:
            insertEdgeProductOf(client, "R00000", "C00000")
        """
        sqlCheck = "SELECT EXPAND(OUT('PRODUCT_OF')) FROM (SELECT FROM Reaction WHERE keggId = '%s')" % (
            source)
        checkCompound = client.query(sqlCheck)
        if len(checkCompound) == 0:
            query = "CREATE EDGE PRODUCT_OF FROM (SELECT * FROM Reaction WHERE keggId = '%s') TO (SELECT * FROM Compound WHERE keggId = '%s')" % (
                source, target)
            client.command(query)
            return
        else:
            return

    def updateCompound(self, client, field, value, where):
        """This method updates a Compound Vertice with a value for a given field
        Args:
            param1 (obj): The client connection from DatabaseClass.
            param2 (str): The name of field
            param3 (str): The value of field
            param4 (str): The where represents the ID condition of the record

        Returns:
            void: void
        Example:
            updateCompound(client, "chebiId", "00000", "00000")
            #UPDATE Compound SET chebiEntries = '00000' WHERE keggId = '00000'
        """
        '''Replace slaches first is important because of the chemical smiles!'''
        value = value.replace("\\", "\\\\")
        value = value.replace("'", "\\'")
        query = "UPDATE Compound SET %s = '%s' WHERE keggId = '%s'" % (
            field, value, where)
        client.command(query)
        return

    def insertEnzyme(self, client, ec, ecClass, ecSubClass, ecSubSubClass):
        """This method inserts a Enzyme Vertice with the ec and its classes
        Args:
            param1 (obj): The client connection from DatabaseClass.
            param2 (str): The ec
            param3 (str): The ec class
            param4 (str): The ec sub class
            param5 (str): The ec sub sub class

        Returns:
            void: void
        Example:
            insertEnzyme(client, "1.2.3.4", "1", "2", "3", "4")
        """
        sqlCheck = "SELECT COUNT(*) as qtd FROM Enzyme WHERE ec = '%s'" % (
            ec)
        checkResult = client.query(sqlCheck)
        for check in checkResult:
            if check.qtd != 0:
                return
            else:
                query = "INSERT INTO Enzyme ( ec, ecClass, ecSubClass, ecSubSubClass ) VALUES ('%s','%s','%s','%s')" % (
                    ec, ecClass, ecSubClass, ecSubSubClass)
                client.command(query)
                return

    def insertEdgeCatalyse(self, client, source, target):
        """This method inserts a CATALYSE Edge into 2path after verifiyng if it does not exists
        Args:
            param1 (obj): The client connection from DatabaseClass.
            param2 (str): The keggId of Enzyme source - source
            param2 (str): The keggId of Reaction target - target
        Returns:
            void: void
        Example:
            insertEdgeCatalyse(client, "1.1.1.1", "R00000")
        """
        verifyQuery = "SELECT COUNT(*) as qtd FROM Reaction WHERE keggId = '%s'" % (target)
        verify = client.query(verifyQuery)
        for reactionExists in verify:
            qtd = reactionExists.qtd
        if qtd > 0:
            dataEnzyme = client.query(
                "SELECT * FROM Enzyme WHERE ec = '%s'" % (source))
            dataReaction = client.query(
                "SELECT * FROM Reaction WHERE keggId = '%s'" % (target))
            sourceEnzyme = ""
            targetReaction = ""
            if dataEnzyme and dataReaction:
                for enzyme in dataEnzyme:
                    sourceEnzyme = enzyme._rid
                for reaction in dataReaction:
                    targetReaction = reaction._rid
                query = "CREATE EDGE CATALYSE FROM " + \
                        sourceEnzyme + " TO " + targetReaction + ""
                client.command(query)
            else:
                return
        return

    def updateReaction(self, client, field, value, where):
        """This method updates a Reaction Vertice into 2path
        Args:
            param1 (obj): The client connection from DatabaseClass.
            param2 (str): The name of field
            param3 (str): The value of field
            param4 (str): The where represents the keggId of the record
        Returns:
            void: void
        Example:
            updateReaction(client, "rheadId", "00000", "R00000")
            #UPDATE Reaction SET rheaId = '0000' WHERE keggId = '00000'
        """
        client.command(
            "UPDATE Reaction SET %s = '%s' WHERE keggId = '%s'" % (field, value, where))
        return
