"""
#####################################################################
#   Organism is a class that provide the target organism manipulation
#
#   @author Waldeyr Mendes Cordeiro da Silva
#   @version Ipê amarelo
#   @place Germany, Leipzig
#   @date 2017-September
#####################################################################
"""

from classes.Config2Path import *


class Organism2Path(Config2Path):
    """Organism is a class that provide the target organism manipulation"""

    def __init__(self, client):
        """
        Construct a new object.
        :return: returns nothing
        """
        Config2Path.__init__(self)
        self._client = client
        self._config = {}
        self._config['taxId'] = None
        self._config['taxName'] = None
        self._config['taxLineage'] = None

    def promptOrganism(self):
        """This method provides organism data prompting the user"""

        print("\nPlease, search for NCBI taxonomy here: " + self.WARNING +
              "https://www.ncbi.nlm.nih.gov/Taxonomy/Browser/wwwtax.cgi" + self.ENDC)
        taxId = input(
            "\nWhat is the NCBI taxonomy ID of your target organism? ")
        reg = re.compile('^[\d\.]+$')
        while not reg.match(taxId):
            taxId = input(
                self.INPUT_INVALID_MSG + "\nPlease, enter the NCBI taxonomy ID: ")
        while (not self.checkValidNcbiTaxId(taxId)):
            taxId = input(
                self.INPUT_INVALID_MSG + "\nPlease, enter the NCBI taxonomy ID: ")
        Entrez.email = '2path@2path.org'
        taxData = Entrez.read(Entrez.efetch(
            id=taxId, db="taxonomy", retmode="xml"))
        self._config['taxId'] = taxData[0]['TaxId']
        self._config['taxName'] = taxData[0]['ScientificName']
        self._config['taxLineage'] = taxData[0]['Lineage'].replace(";", ",")
        return

    def promptBlast(self):
        """
        This method provides blast configs prompting the user
        blastdb    = SwissProt or TrEMBL
        stype      = protein or nucleotide
        alignments = number of alignments in results
        evalue     = evalue
        output     = is automatic, not choosed by user
        """
        self._config["blastdb"] = input(
            "What blast database do you prefer? \n\ts for SwissProt\n\tt for TrEMBL\n\t" + self.INPUT_ARROW)
        reg = re.compile('^[q|s|t]{1}$')  # q or s or t
        while not reg.match(self._config["blastdb"]):
            self._config["blastdb"] = input(
                self.INPUT_INVALID_MSG + "\n\tPlease, enter: \n\ts for SwissProt\n\tt for TrEMBL\n\t" + self.INPUT_ARROW)
        if self._config["blastdb"] == 'q':
            sys.exit("You finished the execution of 2Path.\n")

        '''TYPE OF SEQUENCE'''
        self._config["stype"] = input(
            "What kind of sequence did you provide? \n\tp for protein\n\tn for nucleotide\n\t" + self.INPUT_ARROW)
        reg = re.compile('^[q|p|n]{1}$')  # q or p or n
        while not reg.match(self._config["stype"]):
            self._config["stype"] = input(
                self.INPUT_INVALID_MSG + "\n\tPlease, enter: \n\tp for protein\n\tn for nucleotide\n\t" + self.INPUT_ARROW)
        if self._config["stype"] == 'q':
            sys.exit("You finished the execution of 2Path.\n")

        '''NUMBER OF ALIGNMENTS'''
        self._config["alignments"] = input(
            "Please, enter the maximun number of alignments?\n\t" + self.INPUT_ARROW)
        reg = re.compile('^q|\d+$')  # digits only
        while not reg.match(self._config["alignments"]):
            self._config["alignments"] = input(
                self.INPUT_INVALID_MSG + "\n\tPlease, enter:\n\tMaximun number of alignments?\n\t" + self.INPUT_ARROW)
        if self._config["alignments"] == 'q':
            sys.exit("You finished the execution of 2Path.\n")

        '''E-VALUE'''
        self._config["evalue"] = input(
            "Please, enter the e-value (ex. 0.00001)\n\t" + self.INPUT_ARROW)
        # scientif notation or q
        # q or decimal or sci
        reg = re.compile('^\d+[e|E][+|-].\d*|q|\d+.\d+$')
        while not reg.match(self._config["evalue"].strip()):
            self._config["evalue"] = input(
                self.INPUT_INVALID_MSG + "\n\tPlease, enter the \ne-value (ex. 0.00001)\n\t" + self.INPUT_ARROW)

        '''OUTPUT'''
        self._config["output"] = os.path.join(
            self.OUTPUT_PATH, 'blastOutput.xml')
        return

    def writeOrganismConfigFile(self):
        """
        This method write/overwrite the organism config file
        The name of file it will be given NCBI ID example: 3349.ini
        """
        organismConfigFile = open(os.path.join(self.ORGANISM_CONFIG_PATH,
                                               self._config['taxId'] + '.ini'), 'w')
        organismConfigFile.write("[blast]" + "\n")
        if self._config["blastdb"] == 's':
            blastdbPath = os.path.join(
                self.BLASTDB_PATH, "SwissProt")
            organismConfigFile.write("blastdb = " + blastdbPath + "\n")
            organismConfigFile.write("reviewed = yes\n")
        if self._config["blastdb"] == 't':
            blastdbPath = os.path.join(
                self.BLASTDB_PATH, "TrEMBL")
            organismConfigFile.write("blastdb = " + blastdbPath + "\n")
            organismConfigFile.write("reviewed = no\n")

        organismConfigFile.write("input = " + self._config['input'] + "\n")
        organismConfigFile.write("output = " + self._config['output'] + "\n")
        organismConfigFile.write("stype = " + self._config['stype'] + "\n")
        organismConfigFile.write(
            "alignments = " + self._config['alignments'] + "\n")
        organismConfigFile.write("evalue = " + self._config['evalue'] + "\n")

        organismConfigFile.write("[organism]" + "\n")
        organismConfigFile.write("taxId = " + self._config['taxId'] + "\n")
        organismConfigFile.write("taxName = " + self._config['taxName'] + "\n")
        organismConfigFile.write(
            "taxLineage = " + self._config['taxLineage'] + "\n")
        organismConfigFile.close()
        return

    def cmdBuilder(self):
        """This method build the blast command based on the given params"""
        job = None
        if self._config['stype'] == "p":
            program = "blastp"
        if self._config['stype'] == "n":
            program = "blastx"
        if program == "blastp":
            job = NcbiblastpCommandline(
                query=self.getConfigValue('blast', 'input'),
                db=self.getConfigValue('blast', 'blastdb'),
                num_alignments=self.getConfigValue('blast', 'alignments'),
                evalue=self.getConfigValue('blast', 'evalue'),
                outfmt='5',
                out=self.getConfigValue('blast', 'output')
            )
        if program == "blastx":
            job = NcbiblastxCommandline(
                query=self.getConfigValue('blast', 'input'),
                db=self.getConfigValue('blast', 'blastdb'),
                num_alignments=self.getConfigValue('blast', 'alignments'),
                evalue=self.getConfigValue('blast', 'evalue'),
                outfmt='5',
                out=self.getConfigValue('blast', 'output')
            )
        return job

    def getConfigValue(self, section: str, key: str):
        """
        This method return a value of a give section/key of NCBI_ID.ini file
        """
        configFile = os.path.join(
            self.ORGANISM_CONFIG_PATH, self._config['taxId'] + '.ini')
        cfg = configparser.ConfigParser()
        cfg.read(configFile)
        return cfg.get(section, key)

    def getNcbiData(self):
        """This method retrieves the NCBI data for the organism"""
        Entrez.email = '2path@2path.org'
        taxData = Entrez.read(Entrez.efetch(
            id=self._config['taxId'], db="taxonomy", retmode="xml"))
        if taxData[0]['TaxId'] == self._config['taxId']:
            return taxData
        else:
            return None

    def getUniprotData(self, uniprotEntry: str):

        uniprotData = None
        query = "id:" + uniprotEntry
        fmt = "tab"
        columns = "id,protein names,go-id,ec,comment(SUBCELLULAR LOCATION)"
        uniprot = UniProt(verbose=False)
        uniTabResults = uniprot.search(query, fmt, columns)
        lines = uniTabResults.split('\n')
        for line in lines[1:len(lines) - 1]:
            uniprotData = line.split("\t")
            uniprotData[1] = uniprotData[1].replace("'", "\\'")
            uniprotData[4] = uniprotData[4].replace("'", "\\'")
            # SubcellularLocation treatment
            regex = r"^SUBCELLULAR LOCATION:\s(.*)\{.*\}"
            pattern = re.compile(regex)
            if pattern.match(uniprotData[4]):
                m = re.search(
                    "^SUBCELLULAR LOCATION:\s(.*)\{.*\}", uniprotData[4]).group(1)
                if ',' in m:
                    uniprotData[4] = ';'.join(m.split(","))
                else:
                    uniprotData[4] = m.strip().title()
            else:
                uniprotData[4] = ''
        return uniprotData



    def checkValidNcbiTaxId(self, taxId: str):
        """This method checks if a given NCBI ID is valid
        :param taxId:
        :return:
        """
        Entrez.email = '2path@2path.org'
        taxData = Entrez.read(Entrez.efetch(
            id=taxId, db="taxonomy", retmode="xml"))
        if len(taxData) == 0:
            return False
        else:
            return True

    def checkInputFile(self):
        """
        This method checks if the input file has the correct name
        Obs.: It doesn't verify the content of the file
        """
        try:
            inputPath = os.path.join(
                self.INPUT_PATH, self._config['taxId'] + '.fasta')
            checkFile = Path(inputPath)
            if not checkFile.is_file():
                sys.exit(self.FAIL +
                         "The file name must be the same of your NCBI ID. Example: 3349.fasta" + self.ENDC)
            else:
                self._config['input'] = inputPath
                return True
        except Exception as e:
            sys.exit(self.FAIL +
                     "The file name must be the same of your NCBI ID. Example: 3349.fasta" + self.ENDC)
        return False

    def checkOrganismInDatabase(self, client: object, taxId: str):
        """This method checks if an Organism already exists in the 2path database"""
        sqlCheck = "SELECT COUNT(*) as qtd FROM Organism WHERE organismId = '%s'" % (taxId)
        checkResult = client.query(sqlCheck)
        for check in checkResult:
            if check.qtd != 0:
                return True
            else:
                return False

    def checkEntryInDatabase(self, client: object, uniprotEntry: str):
        """
        This method checks if exists the Entry Vertice in database
        """
        sqlCheck = "SELECT COUNT(*) as qtd FROM Entry WHERE uniprotEntry = '%s'" % (uniprotEntry)
        checkResult = client.query(sqlCheck)
        for check in checkResult:
            if check.qtd != 0:
                return True
            else:
                return False

    def checkEdgeCatalyticActivityInDatabase(self, client: object, source: str, target: str):
        sqlCheck = "SELECT EXPAND(OUT('CATALYTIC_ACTIVITY').ec) FROM (SELECT FROM Entry WHERE uniprotEntry  = '%s')" % (
            source)
        checkResult = client.query(sqlCheck)
        if len(checkResult) != 0:  # exists a CATALYTIC_ACTIVITY edge
            for check in checkResult:
                if check.value != target:  # same source, same target ?
                    return True
                else:  # CATALYTIC_ACTIVITY edge, but for a different target
                    return False
        else:  # Doesn't exists a CATALYTIC_ACTIVITY edge
            return False

    def checkEcInDatabase(self, client: object, ec: str):
        '''This method checks if a given EC exists in 2path'''
        sqlCheck = "SELECT FROM Enzyme WHERE ec = '%s'" % (ec)
        checkResult = client.query(sqlCheck)
        if len(checkResult) == 0:
            return False
        else:
            return True

    def loadEntries(self, taxId: str, uniprotEntry: str, submittedSequenceId: str):
        """
        This method retrieves the uniprot data for each blast hit of the submitted sequences
        """
        inserted = False
        uniprotData = self.getUniprotData(uniprotEntry)
        if uniprotData:
            inserted = self.insertEntry(self._client,
                                        uniprotEntry,
                                        uniprotData[1],
                                        uniprotData[2],
                                        uniprotData[3],
                                        uniprotData[4])
        else:
            pass
        if inserted:
            self.insertEdgeHas(self._client,
                               taxId,
                               uniprotEntry,
                               submittedSequenceId)

            ecs = uniprotData[3].split(';')
            for ec in ecs:
                ec = ec.strip()
                inserted = self.insertEdgeCatalyticActivity(self._client, uniprotEntry, ec)

        return

    def insertEntry(self,
                    client: object,
                    uniprotEntry: str,
                    proteinNames: str,
                    proteinGOs: str,
                    proteinECs: str,
                    subCellularLocations: str):
        """
        This method inserts an Entry Vertice with the given params as properties after verify its previous existence
        :param client:
        :param uniprotEntry:
        :param proteinNames:
        :param proteinGOs:
        :param proteinECs:
        :param subCellularLocations:
        :return:
        """
        if not self.checkEntryInDatabase(client, uniprotEntry):
            query = "INSERT INTO Entry (uniprotEntry, proteinNames, proteinGOs, proteinECs, subCellularLocations) " \
                    "VALUES ('%s','%s','%s','%s','%s')" \
                    % (uniprotEntry, proteinNames, proteinGOs, proteinECs, subCellularLocations)
            client.command(query)
            return True
        else:
            return False

    def insertEdgeHas(self, client: object, source: str, target: str, submittedSequenceId: str):
        """
        This method inserts an HAS Edge between an Organism and an Entry, after verify its previous existence
        :param client:
        :param source:
        :param target:
        :param submittedSequenceId:
        :return:
        """
        sqlCheck = "SELECT EXPAND(IN('HAS').organismId) FROM (SELECT FROM Entry WHERE uniprotEntry = '%s')" % (target)
        checkResult = self._client.query(sqlCheck)

        if len(checkResult) != 0:  # exists a HAS edge to this Entry
            for check in checkResult:
                if check.value != source:  # different source ?
                    query = "CREATE EDGE HAS FROM (SELECT FROM Organism WHERE organismId = '%s') TO (SELECT FROM Entry WHERE uniprotEntry = '%s') SET submittedSequenceId = '%s'" % (
                        source, target, submittedSequenceId)
                    id = client.command(query)
                    # submittedSequenceId
                    query2 = "UPDATE HAS SET submittedSequenceId = '%s' WHERE @rid = '%s'" % (
                        submittedSequenceId, id[0]._rid)
                    client.command(query2)
                    return True
                else:  # same source, same target (repeated?)
                    return False
        else:  # not exists yet
            query = "CREATE EDGE HAS FROM (SELECT FROM Organism WHERE organismId = '%s') TO (SELECT FROM Entry WHERE uniprotEntry = '%s') SET submittedSequenceId = '%s'" % (
                source, target, submittedSequenceId)
            id = client.command(query)
            # submittedSequenceId
            query2 = "UPDATE HAS SET submittedSequenceId = '%s' WHERE @rid = '%s'" % (submittedSequenceId, id[0]._rid)
            client.command(query2)
            return True

    def insertEdgeCatalyticActivity(self, client: object, source: str, target: str):

        if not self.checkEcInDatabase(client, target):  # enzyme doesn't exists yet
            self.insertNewEnzyme(client, target)
        # enzyme already exists

        if not self.checkEdgeCatalyticActivityInDatabase(client, source, target):
            query = "CREATE EDGE CATALYTIC_ACTIVITY FROM (SELECT FROM Entry WHERE uniprotEntry = '%s') TO (SELECT FROM Enzyme WHERE ec = '%s')" % (
                source, target)
            insertedId = client.command(query)
            return insertedId
        else:
            return False

    def insertOrganism(self, client: object, taxData: list):
        """
        This method inserts an Organism Vertice into 2path from given params after verifiy if it doesn't exists
        If exists, it will prompted to the user the option for overwrite it
        """
        taxData[0]['ScientificName'] = taxData[0]['ScientificName'].replace("'", "\\'")
        taxId = taxData[0]['TaxId']
        taxName = taxData[0]['ScientificName']
        taxLineage = taxData[0]['Lineage']

        query = "INSERT INTO Organism (organismId, organismName, organismLineage) VALUES ('%s','%s', '%s')" % (
            taxId, taxName, taxLineage)
        if self.checkOrganismInDatabase(client, taxId):
            answer = input(
                "The organism " + taxId + " already exists. Would you like to " + self.WARNING + "OVERWRITE" + self.ENDC + " it? \n\ty for YES\n\tn for NO\n\t" + self.INPUT_ARROW)
            reg = re.compile('^[q|y|n]{1}$')  # q or y or n
            while not reg.match(answer):
                answer = input(
                    self.INPUT_INVALID_MSG + "\n\tPlease, enter: \n\ty for YES\n\tn for NO\n\t" + self.INPUT_ARROW)
            if answer == 'q' or answer == 'n':
                sys.exit("You finished the execution of 2Path.\n")
            if answer == 'y':
                client.command("TRUNCATE CLASS Organism UNSAFE")
                client.command("TRUNCATE CLASS HAS UNSAFE")
                client.command("TRUNCATE CLASS Entry UNSAFE")
                client.command("TRUNCATE CLASS CATALYTIC_ACTIVITY UNSAFE")
                # New, overwrite
                client.command(query)
            return
        else:
            # New, no overwrite
            client.command(query)
            return

    def insertNewEnzyme(self, client, ec):
        '''
        This method inserts into 2path a new EC and its classes and subclasses
        It occurs if the annotated ec doesn't exists yet
        '''
        regexClass = r"\d\.{1}\-\.{1}\-\.{1}\-"
        regexSubClass = r"\d\.{1}\d+\.{1}\-\.{1}\-"
        regexSubSubClass = r"\d\.{1}\d+\.{1}\d+\.{1}\-"
        patternClass = re.compile(regexClass)
        patternSubClass = re.compile(regexSubClass)
        patternSubSubClass = re.compile(regexSubSubClass)
        ecClassification = ec.split(".")

        filename = 'enzclass.txt'
        enzymesClasses = {}
        f = open(filename)
        lines = f.readlines()
        for line in lines:
            regex = r"^\d\.(\d+|\s)"
            pattern = re.compile(regex)
            if pattern.match(line.strip()):
                k = re.split("\s{2,}", line.strip())[0]
                k = k.replace(" ", "")
                v = re.split("\s{2,}", line.strip())[1]
                v = v.replace(".", "")
                enzymesClasses[k] = v
        f.close()
        ecClass = ''
        ecSubClass = ''
        ecSubSubClass = ''
        for k, v in enzymesClasses.items():
            v = v.replace("'", "\\'")
            refClassification = k.split(".")
            if patternClass.match(k) and refClassification[0] == ecClassification[0]:
                ecClass = v
            if patternSubClass.match(k) and refClassification[0] == ecClassification[0] and refClassification[1] == \
                    ecClassification[1]:
                ecSubClass = v
            if patternSubSubClass.match(k) and refClassification[0] == ecClassification[0] and refClassification[1] == \
                    ecClassification[1] and refClassification[2] == ecClassification[2]:
                ecSubSubClass = v
        query = "INSERT INTO Enzyme (ec, ecClass, ecSubClass, ecSubSubClass) VALUES ('%s','%s','%s','%s')" % (
            ec, ecClass, ecSubClass, ecSubSubClass)
        insertedId = client.command(query)
        return insertedId
