''' 
#####################################################################
#   BlastParser2Path is a class that provide a blast results parser
#   It uses:
#       ConfigClass                             
#                                                                   
#   @author Waldeyr Mendes Cordeiro da Silva                        
#   @version Ipê amarelo                                                   
#   @place Germany, Leipzig                                         
#   @date 2017-September                                               
#####################################################################
'''

from classes.Config2Path import *

class BlastParser2Path(Config2Path):
    """BlastParser2Path is a class that provide a blast results parser"""

    def __init__(self, resultFilePath):
        """
        Construct a new object.
        :return: returns nothing
        """
        resultHandle = open(resultFilePath)
        self._blastResults = NCBIXML.parse(resultHandle)
        self._hits = {}

    def setBlastResults(self, resultFilePath):
        resultHandle = open(resultFilePath)
        self._blastResults = NCBIXML.parse(resultHandle)

    def getBlastResults(self):
        return self._blastResults

    def getHits(self):
        return self._hits

    def addHit(self, uniprotEntry, submittedSequenceId):
        self._hits[uniprotEntry] = [submittedSequenceId]

    def loadHits(self):
        """
        This method get the target data from hits and package them into a dict
        It uses python regex
        """
        regex = r"\|(.*)\|"
        pattern = re.compile(regex)
        # Get tuple <uniprotEntry, submittedSequenceId>
        for blastResult in self.getBlastResults():
            blastResult.query = blastResult.query.replace("'", "\\'")
            for alignment in blastResult.alignments:
                self.addHit(
                    pattern.findall(alignment.hit_def)[0],
                    blastResult.query
                )
        return
