"""
#####################################################################
#   Database is a class that provide the database manipulation
#
#   @author Waldeyr Mendes Cordeiro da Silva
#   @version Ipê amarelo
#   @place Germany, Leipzig
#   @date 2017-September
#####################################################################
"""

from classes.Config2Path import *
from pyorient import PyOrientDatabaseException

class Database2Path(Config2Path):
    """
    Database is a class that provide the database manipulation
    """
    CLIENT = None

    def __init__(self):
        """
        Construct a new 'Database2Path' object.
        :return: None
        """
        Config2Path.__init__(self)

        self._host = None
        self._port = None
        self._user = None
        self._password = None
        self._db = None

    def loadDatabaseConfiguration(self):
        self._host = self.getConfigValue('orientdb', 'host')
        self._port = int(self.getConfigValue('orientdb', 'port'))
        self._user = self.getConfigValue('orientdb', 'user')
        self._password = self.getConfigValue('orientdb', 'password')
        self._db = self.getConfigValue('orientdb', 'db')
        return

    def setDatabaseConfiguration(self):
        """This method provides organism data prompting the user"""

        self._host = input(
            "\nPlease, enter the hostname of OrientDB server: ")
        reg = re.compile(
            "^(([a-zA-Z0-9]|[a-zA-Z0-9][a-zA-Z0-9\-]*[a-zA-Z0-9])\.)*([A-Za-z0-9]|[A-Za-z0-9][A-Za-z0-9\-]*[A-Za-z0-9])$")
        while not reg.match(self._host):
            self._host = input(
                self.INPUT_INVALID_MSG + "\nPlease, enter a valid hostname: ")

        self._port = input(
            "\nPlease, enter the port number for OrientDB server: ")
        reg = re.compile("^\d+$")
        while not reg.match(self._port):
            self._port = input(
                self.INPUT_INVALID_MSG + "\nPlease, enter a valid port number for OrientDB server: ")

        self._user = input(
            "\nPlease, enter the username of OrientDB server: ")
        reg = re.compile("^[0-9a-zA-Z$_]+$")
        while not reg.match(self._user):
            self._user = int(input(
                self.INPUT_INVALID_MSG + "\nPlease, enter a valid username for OrientDB server: "))

        self._password = input(
            "\nPlease, enter the password of OrientDB server: ")
        reg = re.compile("^\S+$")
        while not reg.match(self._password):
            self._password = int(input(
                self.INPUT_INVALID_MSG + "\nPlease, enter a valid password for OrientDB server: "))

        self._db = '2path'

        self.writeDatabaseConfigFile(
            self._host, self._port, self._user, self._password, self._db)

    def writeDatabaseConfigFile(self, host, port, user, password, db):
        """
        This method write a content into the 2path.ini file according to the given params
        :param host: String
        :param port: int
        :param user: String
        :param password: String
        :param db: String
        :return: None
        """
        try:
            databaseConfigFile = open(Config2Path.DATABASE_CONFIG_PATH, 'w')
            databaseConfigFile.write("[orientdb]" + "\n")
            databaseConfigFile.write("host = " + host + "\n")
            databaseConfigFile.write("port = " + str(port) + "\n")
            databaseConfigFile.write("user = " + user + "\n")
            databaseConfigFile.write("password = " + password + "\n")
            databaseConfigFile.write("db = " + db + "\n")
            databaseConfigFile.close()
        except Exception as e:
            print(Config2Path.FAIL, "Error: ", e, Config2Path.ENDC)
        return

    def getConnection(self):
        """
        This method provide a generic connection (no specific database) for OrientDB according to the 2path.ini file configs
        :return: pyorient Object
        """
        self.CLIENT = pyorient.OrientDB(self._host, int(self._port))
        self.CLIENT.connect(self._user, self._password)
        return self.CLIENT

    def open2Path(self):
        """
        This method provide a connection with the OrientDB 2path database according to the 2path.ini file configs
        :return: pyorient Object
        """
        self.CLIENT = pyorient.OrientDB(self._host, int(self._port))
        self.CLIENT.db_open(self._db, self._user, self._password)
        return self.CLIENT

    def close2Path(self):
        '''
        This method closes an open connection
        '''
        self.CLIENT.close()

    def testConnection(self):
        try:
            self.open2Path()
            self.close2Path()
            return True
        except Exception as e:
            print(self.WARNING + "Connection error:" + self.ENDC)
            if hasattr(e, 'message'):
                print(e.message)
                return False
            else:
                print(e)
                return False

    def getConfigValue(self, section, key):
        """
        This method return a value of a give section/key of 2path.ini file
        :param section: String
        :param key: String
        :return: String
        """
        configFile = self.DATABASE_CONFIG_PATH
        cfg = configparser.ConfigParser()
        cfg.read(configFile)
        return cfg.get(section, key)

    def checkDatabase(self, client):
        """
        This method checks if the 2path database already exists
        :param client: pyorient Object
        :return: Boolean
        """
        if client.db_exists(self._db, pyorient.STORAGE_TYPE_PLOCAL):
            return True
        else:
            return False

    def createPropertie(self, verticeClass, propertieName, propertieType, mandatory):
        """
        This method excutes a SQL command to create a propertie for a given vertice
            :param verticeClass: String
            :param propertieName: String
            :param propertieType: String
            :param mandatory: Boolean
            :return: None
        Example:
            >>>createPropertie("Compound", "keggId", "String", "True")
            CREATE PROPERTY Compound.keggId String (MANDATORY True)
        """
        self.CLIENT.command("CREATE PROPERTY " + verticeClass + "." + propertieName +
                            " " + propertieType + " (MANDATORY " + mandatory + ")")
        return

    def checkAndDropSchema(self):
        print(self.WARNING + "The 2path database schema already exists." + self.ENDC)
        answer = input(
            "You will need to " + self.WARNING + "DROP" + self.ENDC +
            " 2path schema to reload it. Would you like do do this? "
            "\n\ty for YES\n\tn for NO\n\t" + self.INPUT_ARROW)
        reg = re.compile('^[q|y|n]{1}$')  # q or y or n
        while not reg.match(answer):
            answer = input(
                self.INPUT_INVALID_MSG + "\n\tPlease, enter: "
                                         "\n\ty for YES\n\tn for NO\n\t" + self.INPUT_ARROW)

            if answer == 'q' or answer == 'n':
                print ("nonono")
                sys.exit("You finished the execution of 2Path.\n")
            if answer == 'y':
                print ("yeyeye")
                print(self.WARNING + "Dropping 2path Database..." + self.ENDC)
                self.CLIENT.db_drop("2path")
                print(self.WARNING + "Done." + self.ENDC)


    def createDatabaseSchema(self):
        """
        This method creates the 2path database schema
        It is used in case of UPGRADE of original database
        :return: None
        """
        
        # overwrite existing 2path schema
        if self.checkDatabase(self.CLIENT):
            self.checkAndDropSchema()

        self.CLIENT.db_create(self._db, pyorient.DB_TYPE_GRAPH,
                              pyorient.STORAGE_TYPE_PLOCAL)
        self.open2Path()
        print('Creating ', self._db, 'schema...')
        print('.', end='', flush=True)
        # CREATE V classes
        # REACTION
        self.CLIENT.command("CREATE class Reaction extends V")
        self.createPropertie("Reaction", "keggId", "String", "True")
        self.createPropertie("Reaction", "rheaId", "String", "False")
        self.createPropertie("Reaction", "pathway", "String", "False")
        print('.', end='', flush=True)

        # COMPOUND
        self.CLIENT.command("CREATE class Compound extends V")
        self.createPropertie("Compound", "keggId", "String", "True")
        self.createPropertie("Compound", "name", "String", "False")
        self.createPropertie("Compound",
                             "iupacName", "String", "False")
        self.createPropertie("Compound", "chebiId", "String", "False")
        self.createPropertie("Compound",
                             "chebiFormula", "String", "False")
        self.createPropertie("Compound",
                             "chebiSmiles", "String", "False")
        self.createPropertie("Compound",
                             "chebiMass", "String", "False")
        self.CLIENT.command("CREATE INDEX Compound.keggId UNIQUE")
        print('.', end='', flush=True)

        # ENZYME
        self.CLIENT.command("CREATE class Enzyme extends V")
        self.createPropertie("Enzyme", "ec", "String", "True")
        self.createPropertie("Enzyme", "ecClass", "String", "False")
        self.createPropertie("Enzyme", "ecSubclass", "String", "False")
        self.createPropertie("Enzyme", "ecSubSubclass", "String", "False")
        self.CLIENT.command("CREATE INDEX Enzyme.ec UNIQUE")
        print('.', end='', flush=True)

        # ENTRY
        self.CLIENT.command("CREATE class Entry extends V")
        self.createPropertie("Entry", "uniprotEntry", "String", "True")
        self.createPropertie("Entry", "proteinNames", "String", "False")
        self.createPropertie("Entry", "proteinGOs", "String", "False")
        self.createPropertie("Entry", "proteinECs", "String", "False")
        self.createPropertie(
            "Entry", "subCellularLocations", "String", "False")
        self.CLIENT.command("CREATE INDEX Entry.uniprotEntry UNIQUE")
        print('.', end='', flush=True)

        # ORGANISM
        self.CLIENT.command("CREATE class Organism extends V")
        self.createPropertie("Organism",
                             "organismId", "String", "True")
        self.createPropertie("Organism",
                             "organismName", "String", "True")
        self.createPropertie("Organism",
                             "organismLineage", "String", "True")
        self.CLIENT.command("CREATE INDEX Organism.organismId UNIQUE")
        self.CLIENT.command("CREATE INDEX Organism.organismName UNIQUE")
        print('.', end='', flush=True)

        # EDGES CLASSES
        self.CLIENT.command("CREATE class REACTANTS extends E ABSTRACT")
        self.CLIENT.command("CREATE class SUBSTRATE_FOR extends REACTANTS")
        self.CLIENT.command("CREATE class PRODUCT_OF extends REACTANTS")

        self.CLIENT.command("CREATE class CATALYSE extends E")
        self.CLIENT.command("CREATE class CATALYTIC_ACTIVITY extends E")
        self.CLIENT.command("CREATE class HAS extends E")
        self.createPropertie(
            "HAS", "submittedSequenceId", "String", "True")
        self.createPropertie(
            "HAS", "submittedSequence", "String", "False")
        print('.', end='', flush=True)
        self.close2Path()
        return


    def truncateCore(self):
        """
        in development
        This method trucate the core database of 2path
        It is used in case of UPDATE
        :return: None
        """
        self.open2Path()
        self.CLIENT.command("DELETE VERTEX Compound")
        self.CLIENT.command("DELETE VERTEX Reaction")
        # self.CLIENT.command("DELETE VERTEX Enzyme")
        self.close2Path()
        # REFAZER AS ARESTAS DE ENTRY PARA ENZYME ou ZERAR O BANCO?
        return
