"""
#####################################################################
#   Reconstruct is a class that provide metabolic network reconstruction fo the given organisms
#
#   @author Waldeyr Mendes Cordeiro da Silva
#   @version Ipê amarelo
#   @place Germany, Leipzig
#   @date 2017-August
#####################################################################
"""

from classes.BlastParser2Path import *
from classes.Organism2Path import *
from classes.Database2Path import *
from bioservices import UniProt


class RunReconstruction(Config2Path):
    """Reconstruct is a class that provide metabolic network reconstruction fo the given organisms"""

    def __init__(self):
        """
        Construct a new 'Database' object.
        :return: returns nothing
        """
        Config2Path.__init__(self)

    def run(self):
        """
        This method executes the reconstruction af a metabolic network, for a given organism
        """
        start_time = time.monotonic()
        print(self.TREE)
        '''Database setup'''
        database = Database2Path()
        database.loadDatabaseConfiguration()
        if database.testConnection():
            print(self.GREEN + "2Path terpenoid metabolic reconstruction" + self.ENDC)
        CLIENT = database.open2Path()
        organism = Organism2Path(CLIENT)

        organism.promptOrganism()
        # check the fasta file name and location
        organism.checkInputFile()
        # user prompt
        organism.promptBlast()
        # config writing
        organism.writeOrganismConfigFile()
        # get NCBI data for the organism
        taxData = organism.getNcbiData()
        organism.insertOrganism(CLIENT, taxData)
        '''BLAST RUN'''
        # building blast command
        blastCmd = organism.cmdBuilder()
        # Truncate resut file
        filename = organism.getConfigValue('blast', 'output')
        open(filename, 'w').close()
        print("Executing Blast. It can take some time...")
        # Writing result xml file
        stdout, stderr = blastCmd()
        '''BLAST RESULT PARSER'''
        # parser object
        parser = BlastParser2Path(filename)
        # loading hits
        parser.loadHits()
        # getting Uniprot data from each hit and inserting them into 2path
        for uniprotEntry, submittedSequenceId in parser.getHits().items():
            print('.', end='', flush=True)
            organism.loadEntries(organism.getConfigValue('organism', 'taxId'), uniprotEntry, submittedSequenceId[0])

        database.close2Path()
        end_time = time.monotonic()
        print("\nAll done. Time: ", timedelta(seconds=end_time - start_time))


rec = RunReconstruction()
rec.run()
