"""
#####################################################################
#   Creation of Blast databases SwissProt and TrEMBL for Viridiplantae
#
#   @author Waldeyr Mendes Cordeiro da Silva
#   @version Ipê amarelo
#   @place Germany, Leipzig
#   @date 2017-September
#####################################################################
"""

from classes.Config2Path import *


class RunBlastConfiguration(Config2Path):
    """Creation of Blast databases SwissProt and TrEMBL for Viridiplantae"""

    def __init__(self):
        """
        Construct a new object.
        :return: returns nothing
        """
        Config2Path.__init__(self)

    def run(self):
        print(self.BOLD+ "\nTaxonomy ID: 327148")
        print("Taxonomy Name: Copaifera officinalis")
        print("Data type: nucleotide\n"+self.ENDC)

        os.system("python3 runReconstruction.py")

runnner = RunBlastConfiguration()
runnner.run()
