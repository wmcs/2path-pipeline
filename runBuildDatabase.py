"""
#####################################################################
#   Configuration of database connection by config/2Path.ini
#
#   @author Waldeyr Mendes Cordeiro da Silva
#   @version Ipê amarelo
#   @place Germany, Leipzig
#   @date 2017-September
#####################################################################
"""
from classes.Database2Path import *
from classes.Core2Path import *


class RunBuildDatabase(Config2Path):
    """Configuration of database connection by config/2Path.ini"""

    def __init__(self):
        """
        Construct a new object.
        :return: returns nothing
        """
        Config2Path.__init__(self)

    def run(self):
        """
        This method executes the reconstruction af a metabolic network, for a given organism
        """
        print(self.TREE)
        '''Database setup'''
        database = Database2Path()
        database.setDatabaseConfiguration()
        database.loadDatabaseConfiguration()
        database.createDatabaseSchema()
        CLIENT = database.open2Path()
        core2path = Core2Path(CLIENT)
        core2path.loadKeggData()

runner = RunBuildDatabase()
runner.run()
